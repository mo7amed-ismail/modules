from odoo import models, fields, api


class partner(models.Model):
    _inherit = 'res.partner'

    da3wa = fields.Boolean()
    instructor=fields.Boolean('Instructor' , default=False)
    session_ids=fields.Many2many('openacademy.session' ,
                                 string='Attended Sessions' ,
                                 readonly=True)
